<?php

/**
 * Class Inchoo_StoreReview_Block_Adminhtml_Review_Edit_Form
 */
class Inchoo_StoreReview_Block_Adminhtml_Review_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * @return Mage_Adminhtml_Block_Widget_Form
     * @throws Exception
     * @throws Mage_Core_Model_Store_Exception
     */
    protected function _prepareForm()
    {

        # create the form with the essential information, such as DOM ID, action
        # attribute, method and the enc type (this is needed if you have image
        # inputs in your form, and doesn't hurt to use otherwise)
        $form = new Varien_Data_Form(
            array(
                'id' => 'edit_form',
                'action' => $this->getUrl('*/*/save', array('id' => $this->getRequest()->getParam('id'))),
                'method' => 'post',
                'enctype' => 'multipart/form-data'
            )
        );
        $form->setUseContainer(true);
        $this->setForm($form);


        # add a fieldset, this returns a Varien_Data_Form_Element_Fieldset object
        $fieldset = $form->addFieldset(
            'base_fieldset',
            array(
                'legend' => Mage::helper('inchoo_storereview')->__('General Information'),
            )
        );
        # now add fields on to the fieldset object, for more detailed info
        # see https://makandracards.com/magento/12737-admin-form-field-types
        $fieldset->addField(
            'title', # the input id
            'text', # the type
            array(
                'label' => Mage::helper('inchoo_storereview')->__('Title'),
                'class' => 'required-entry',
                'required' => true,
                'name' => 'title',
            )
        );
        $fieldset->addField(
            'description',
            'textarea',
            array(
                'label' => Mage::helper('inchoo_storereview')->__('Description'),
                'name' => 'description',
                'required' => true
            )
        );


        $fieldset->addField('status', 'select', array(
            'label' => Mage::helper('inchoo_storereview')->__('Status'),
            'class' => 'required-entry',
            'required' => true,
            'name' => 'status',
            'onclick' => "",
            'onchange' => "",
            'value' => '1',
            'values' => Mage::getModel('inchoo_storereview/review')->getReviewStatusesOptionArray(),
            'disabled' => false,
            'readonly' => false,
            'tabindex' => 1
        ));

        # we can use multiple fieldsets
        $fieldset = $form->addFieldset(
            'rating_fieldset',
            array(
                'legend' => Mage::helper('inchoo_storereview')->__('Rating'),
            )
        );


        $fieldset->addField('rating', 'radios', array(
            'label' => Mage::helper('inchoo_storereview')->__('Rating'),
            'name' => 'rating',
            'onclick' => "",
            'onchange' => "",
            'value' => '20',
            'values' => Mage::getModel('inchoo_storereview/review')->getReviewRatingsOptionArray()
        ));


        # we can use multiple fieldsets
        $fieldset = $form->addFieldset(
            'store_fieldset',
            array(
                'legend' => Mage::helper('inchoo_storereview')->__('Store'),
            )
        );


        /**
         * Check is single store mode
         */
        if (!Mage::app()->isSingleStoreMode()) {
            $field = $fieldset->addField('store_id', 'select', array(
                'name' => 'store_id',
                'label' => Mage::helper('cms')->__('Store View'),
                'title' => Mage::helper('cms')->__('Store View'),
                'required' => true,
                'values' => Mage::getSingleton('adminhtml/system_store')->getStoreValuesForForm(false, true),
            ));
            $renderer = $this->getLayout()->createBlock('adminhtml/store_switcher_form_renderer_fieldset_element');
            $field->setRenderer($renderer);
        } else {
            $fieldset->addField('store_id', 'hidden', array(
                'name' => 'store_id',
                'value' => Mage::app()->getStore(true)->getId()
            ));
        }


        $fieldset->addField('review_id', 'hidden', array(
            'name' => 'review_id',
        ));

        # we can use multiple fieldsets
        $fieldset = $form->addFieldset(
            'customer_fieldset',
            array(
                'legend' => Mage::helper('inchoo_storereview')->__('Customer'),
            )
        );

        $fieldset->addField('customer_id', 'select', array(
            'label' => Mage::helper('inchoo_storereview')->__('Customer'),
            'class' => 'required-entry',
            'required' => true,
            'name' => 'customer_id',
            'onclick' => "",
            'onchange' => "",
            'value' => '1',
            'values' => Mage::getModel('inchoo_storereview/review')->getCustomersToOptionsArray(),
            'disabled' => false,
            'readonly' => false,
            'tabindex' => 1
        ));


        Mage::registry('store_review_data') ? $form->setValues(Mage::registry('store_review_data')->getData()) : null;

        return parent::_prepareForm();
    }
}