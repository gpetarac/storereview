<?php

/* @var $installer Mage_Core_Model_Resource_Setup */

$installer = $this;

$installer->startSetup();

$table = $installer->getConnection()
    ->newTable($installer->getTable('inchoo_store_review'))
    ->addColumn('review_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null,
        array(
            'identity' => true,
            'unsigned' => true,
            'primary' => true,
            'nullable' => false
        ), 'Review ID')
    ->addColumn('customer_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null,
        array(
            'unsigned' => true,
            'nullable' => false
        ), 'Customer ID')
    ->addColumn('store_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, 5,
        ['identity' => false, 'unsigned' => true, 'nullable' => false, 'primary' => false]
        , 'Store ID')
    ->addColumn('title', Varien_Db_Ddl_Table::TYPE_TEXT, 255,
        array(
            'nullable' => false
        ), 'title')
    ->addColumn('description', Varien_Db_Ddl_Table::TYPE_TEXT, 1024,
        array(
            'nullable' => false
        ), 'Description')
    ->addColumn('rating', Varien_Db_Ddl_Table::TYPE_SMALLINT, 3,
        array(
            'default' => 0
        ), 'Rating(%)')
    ->addColumn('status', Varien_Db_Ddl_Table::TYPE_TEXT, 12,
        array(
            'default' => 'Pending'
        ), 'Status')
    ->addColumn(
        'created_at',
        Varien_Db_Ddl_Table::TYPE_TIMESTAMP,
        null,
        ['nullable' => false, 'default' => Varien_Db_Ddl_Table::TIMESTAMP_INIT], 'Created At')
    ->addColumn(
        'updated_at',
        Varien_Db_Ddl_Table::TYPE_TIMESTAMP,
        null,
        ['nullable' => false, 'default' => Varien_Db_Ddl_Table::TIMESTAMP_INIT_UPDATE], 'Updated At')
    ->setComment('Inchoo Store Review');


$installer->getConnection()->createTable($table);
$installer->endSetup();

////////////////////////// ADDING RELATIONSHIP ///////////////////////////////////////////////////////////////////
$installer = $this;
$installer->startSetup();

$shopReviewTable = $installer->getTable('inchoo_store_review');
$storeTable = $installer->getTable('core/store');
$customerEntityTable = $installer->getTable('customer/entity');


$installer->getConnection()->addForeignKey(
    $installer->getFkName($shopReviewTable, 'store_id', $storeTable, 'store_id'),
    $shopReviewTable,
    'store_id',
    $storeTable,
    'store_id',
    Varien_Db_Ddl_Table::ACTION_CASCADE
);


$installer->getConnection()->addForeignKey(
    $installer->getFkName($shopReviewTable, 'customer_id', $customerEntityTable, 'entity_id'),
    $shopReviewTable,
    'customer_id',
    $customerEntityTable,
    'entity_id',
    Varien_Db_Ddl_Table::ACTION_CASCADE
);


$installer->endSetup();
