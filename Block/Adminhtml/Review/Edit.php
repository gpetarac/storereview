<?php

/**
 * Class Inchoo_StoreReview_Block_Adminhtml_Review_Edit
 */
class Inchoo_StoreReview_Block_Adminhtml_Review_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    /**
     * Class constructor
     */
    public function __construct()
    {
        $this->_objectId = 'review_id'; # this is the param we look for in the url to load the entity
        $this->_controller = 'adminhtml_review'; # same as the grid container
        $this->_blockGroup = 'inchoo_storereview'; # same as the grid container
        $this->_updateButton('save', 'label', Mage::helper('inchoo_storereview')->__('Save Review')); # sets the text for the save button
        $this->_updateButton('delete', 'label', Mage::helper('inchoo_storereview')->__('Delete Review')); # sets the text for the delete button
        parent::__construct(); # for form containers, parent constructor should be called first

    }

    /**
     * Get header text
     *
     * @return string
     */
    public function getHeaderText()
    {
        # the text changes depending on if we are editing an existing entity
        # or creating a new one, this is based on a registry item that the
        # controller registers, this will never exist in our examples as we
        # don't have the controller logic to support it
        if (Mage::registry('store_review_data') && Mage::registry('store_review_data')->getId()) {
            return Mage::helper('inchoo_storereview')->__("Edit existing review");
        } else {
            return Mage::helper('inchoo_storereview')->__('Create new review');
        }
    }

    /**
     * Header CSS class
     *
     * Used to set the icon next to the header text, not at all needed but a
     * nice touch. Look at all the headers to see the available icons, or make
     * your own by omitting this and making a CSS rule for .head-adminhtml-thing
     *
     * @return string The CSS class
     */
    public function getHeaderCssClass()
    {
        return 'icon-head head-cms-page';
    }
}