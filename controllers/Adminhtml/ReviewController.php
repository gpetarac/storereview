<?php

/**
 * Class Inchoo_StoreReview_Adminhtml_ReviewController
 */
class Inchoo_StoreReview_Adminhtml_ReviewController extends Mage_Adminhtml_Controller_Action
{

    /**
     * Initial action - shows grid
     */
    public function indexAction()
    {
        $this->loadLayout()->_setActiveMenu('report/review/inchoo_storereview');
        $this->renderLayout();
    }


    /**
     * Creating new review action
     */
    public function newAction()
    {
        $this->loadLayout()->_setActiveMenu('report/review/inchoo_storereview');
        $this->renderLayout();
    }


    /**
     * Editing existing review
     * @throws Mage_Core_Exception
     */
    public function editAction()
    {
        $this->loadLayout()->_setActiveMenu('report/review/inchoo_storereview');

        $reviewId = $this->getRequest()->getParam('review_id');
        $model = Mage::getModel('inchoo_storereview/review')->load($reviewId);

        if ($model->getId() || $reviewId == 0) {

            $data = Mage::getSingleton('adminhtml/session')->getFormData(true);

            $model->setData($data);

            Mage::register('store_review_data', $model);

        }

        $this->renderLayout();
    }


    /**
     * Saving review to db
     * @return Mage_Core_Controller_Varien_Action
     */
    public function saveAction()
    {

        $reviewData = $this->getRequest()->getPost();

        /** @var \Inchoo_StoreReview_Model_Review $reviewModel */
        $reviewModel = Mage::getModel('inchoo_storereview/review')->load($reviewData['review_id']);;


        $reviewModel->setData($reviewData);

        try {

            $reviewModel->save();

            Mage::getSingleton('core/session')->addSuccess(Mage::helper('inchoo_storereview')->__('Your review has been saved!'));

        } catch (\Exception $e) {

            Mage::getSingleton('core/session')->addError(Mage::helper('inchoo_storereview')->__($e->getMessage()));

        }

        return $this->_redirect('*/*/');
    }


    /**
     * Deletes single review
     * @return $this|Mage_Core_Controller_Varien_Action
     */
    public function deleteAction()
    {

        try {

            $reviewModel = Mage::getModel('inchoo_storereview/review')->load($this->getRequest()->getParam('review_id'));

            $reviewModel->delete();

            Mage::getSingleton('core/session')->addSuccess(Mage::helper('inchoo_storereview')->__('Review has been deleted'));


        } catch (\Exception $e) {

            Mage::getSingleton('core/session')->addError(Mage::helper('inchoo_storereview')->__('Review could not be deleted'));

        }

        return $this->_redirect('*/*/');

    }


    /**
     * Action for deleting multiple review records form db
     */
    public function massDeleteAction()
    {

        $requestIds = $this->getRequest()->getParam('review');

        if (!is_array($requestIds)) {

            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select reqeust(s)'));

        } else {

            try {

                foreach ($requestIds as $review_id) {

                    $reviewModel = Mage::getModel('inchoo_storereview/review')->load($review_id);
                    $reviewModel->delete();
                }

                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('adminhtml')->__(
                        'Total of %d record(s) were successfully deleted', count($requestIds)
                    )
                );

            } catch (Exception $e) {

                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }

        $this->_redirect('*/*/');
    }


}