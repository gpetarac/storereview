<?php

/**
 * Class Inchoo_StoreReview_ReviewController
 */
class Inchoo_StoreReview_ReviewController extends Mage_Core_Controller_Front_Action
{

    /**
     * @return Mage_Core_Controller_Front_Action|void
     */
    public function preDispatch()
    {
        parent::preDispatch();

        if (!Mage::getSingleton('customer/session')->authenticate($this)) {
            $this->setFlag('', self::FLAG_NO_DISPATCH, true);
        }
    }


    /**
     * @return mixed
     * @throws Mage_Core_Model_Store_Exception
     */
    private function getReview()
    {

        return Mage::getModel('inchoo_storereview/review')
            ->getCollection()
            ->addFieldToFilter('customer_id', Mage::getSingleton('customer/session')->getId())
            ->addFieldToFilter('store_id', Mage::app()->getStore()->getId())
            ->getFirstItem();

    }


    /**
     * Setting review object to register
     * @throws Mage_Core_Exception
     * @throws Mage_Core_Model_Store_Exception
     */
    private function registerReview()
    {
        Mage::register('store_review', $this->getReview());
    }


    /**
     * Sets active store review link in customer navigation
     */
    private function setStoreLinkActive()
    {
        $navigationBlock = $this->getLayout()->getBlock('customer_account_navigation');

        if ($navigationBlock) {
            $navigationBlock->setActive('review/review/show');
        }
    }

    /**
     * Show first page (customer review)
     * @throws Mage_Core_Exception
     * @throws Mage_Core_Model_Store_Exception
     */
    public function showAction()
    {
        $this->loadLayout();
        $this->registerReview();
        $this->renderLayout();

    }


    /**
     * Renders review form
     * @throws Mage_Core_Exception
     * @throws Mage_Core_Model_Store_Exception
     */
    public function createAction()
    {
        $this->loadLayout();

        $this->setStoreLinkActive();
        $this->registerReview();

        $this->renderLayout();

    }


    /**
     * Renders review form
     * @return Mage_Core_Controller_Varien_Action
     * @throws Mage_Core_Exception
     * @throws Mage_Core_Model_Store_Exception
     */
    public function editAction()
    {
        $this->loadLayout();

        if (!$this->getReview()->getId()) {

            Mage::getSingleton('core/session')->addNotice(Mage::helper('inchoo_storereview')->__('There is nothing to edit'));
            return $this->_redirectError(Mage::getUrl('*/*/show'));
        }

        $this->setStoreLinkActive();
        $this->registerReview();
        $this->renderLayout();

    }


    /**
     * @return Mage_Core_Controller_Varien_Action
     * @throws Mage_Core_Model_Store_Exception
     * @throws Zend_Validate_Exception
     */
    public function saveAction()
    {
        if (!$this->getRequest()->isPost()) {

            return $this->_redirectError(Mage::getUrl('*/*/'));
        }

        $reviewData = $this->getRequest()->getPost();

        /** @var \Inchoo_StoreReview_Model_Review $reviewModel */
        $reviewModel = Mage::getModel('inchoo_storereview/review');

        if ($reviewModel->validate($reviewData) && !$this->_validateFormKey()) {
            return $this->_redirect('*/*/');
        }


        if (!empty($reviewData['review_id'])) {
            $reviewModel->setReviewId($reviewData['review_id']);
        }

        $reviewModel->setTitle($reviewData['title']);
        $reviewModel->setDescription($reviewData['description']);
        $reviewModel->setRating($reviewData['rating']);
        $reviewModel->setCustomerId(Mage::getSingleton('customer/session')->getId());
        $reviewModel->setStoreId(Mage::app()->getStore()->getId());

        try {

            $reviewModel->save();

            Mage::getSingleton('core/session')->addSuccess(Mage::helper('inchoo_storereview')->__('Your review has been saved!'));

        } catch (\Exception $e) {

            Mage::getSingleton('core/session')->addError(Mage::helper('inchoo_storereview')->__('Review not saved!'));

        }

        return $this->_redirectUrl('/review/review/show');
    }


    /**
     * @return Mage_Core_Controller_Varien_Action
     */
    public function deleteAction()
    {
        try {

            $this->getReview()->delete();

            Mage::getSingleton('core/session')->addSuccess(Mage::helper('inchoo_storereview')->__('Review has been deleted'));


        } catch (\Exception $e) {

            Mage::getSingleton('core/session')->addError(Mage::helper('inchoo_storereview')->__('Review could not be deleted'));

        }

        return $this->_redirectUrl('/review/review/show');

    }


}