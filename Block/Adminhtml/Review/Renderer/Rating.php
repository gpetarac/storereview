<?php

/**
 * Class Inchoo_StoreReview_Block_Adminhtml_Review_Renderer_Rating
 */
class Inchoo_StoreReview_Block_Adminhtml_Review_Renderer_Rating extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    /**
     * @param Varien_Object $row
     * @return string
     */
    public function render(Varien_Object $row)
    {

        $ratingValue = $row->getData($this->getColumn()->getIndex());

        $reviewRatings = Inchoo_StoreReview_Model_Review::getReviewRatings();

        return $reviewRatings[$ratingValue];

    }

}