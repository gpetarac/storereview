<?php

/**
 * Class Inchoo_StoreReview_Block_Adminhtml_Review_Grid
 */
class Inchoo_StoreReview_Block_Adminhtml_Review_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * Class constructor
     */
    public function __construct()
    {
        parent::__construct(); # for grids, parent constructor should be called first
        $this->setId('reviewGrid'); # not sure where the grid id gets used
        $this->setDefaultSort('updated_at'); # sets the default sort column
        $this->setDefaultDir('desc'); # sets the default sort direction
        $this->setSaveParametersInSession(true); # this sets filters and sorts in the session, as opposed to using the url
    }

    /**
     * @return $this|Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareCollection()
    {
        /** @var \Inchoo_StoreReview_Model_Resource_Review_Collection $collection */
        $collection = Mage::getModel('inchoo_storereview/review')
            ->getCollection();

        $firstName = Mage::getModel('eav/entity_attribute')->loadByCode('1', 'firstname');
        $lastNAme = Mage::getModel('eav/entity_attribute')->loadByCode('1', 'lastname');

        $collection->getSelect()
            ->join(array('ce1' => 'customer_entity_varchar'), 'ce1.entity_id=main_table.customer_id', array('firstname' => 'value'))
            ->where('ce1.attribute_id=' . $firstName->getAttributeId())
            ->join(array('ce2' => 'customer_entity_varchar'), 'ce2.entity_id=main_table.customer_id', array('lastname' => 'value'))
            ->where('ce2.attribute_id=' . $lastNAme->getAttributeId())
            ->columns(new Zend_Db_Expr("CONCAT(`ce1`.`value`, ' ',`ce2`.`value`) AS customer_name"));


        $collection->setOrder('updated_at', 'desc');
        $this->setCollection($collection);
        parent::_prepareCollection();
        return $this;
    }


    /**
     * @return $this
     * @throws Exception
     */
    protected function _prepareColumns()
    {
        $this->addColumn('title', array(
            'header' => Mage::helper('inchoo_storereview')->__('Title'),
            'index' => 'title',
            'type' => 'text'
        ));

        $this->addColumn('description', array(
            'header' => Mage::helper('inchoo_storereview')->__('Description'),
            'index' => 'description',
            'type' => 'text'
        ));

        $this->addColumn('customer_name', array(
            'header' => Mage::helper('inchoo_storereview')->__('Customer'),
            'index' => 'customer_name',
            'type' => 'text'
        ));


        $this->addColumn('rating', array(
            'header' => Mage::helper('inchoo_storereview')->__('Rating'),
            'index' => 'rating',
            'type' => 'text',
            'renderer' => 'Inchoo_StoreReview_Block_Adminhtml_Review_Renderer_Rating',// THIS IS WHAT THIS POST IS ALL ABOUT
        ));

        $this->addColumn('status', array(
            'header' => Mage::helper('inchoo_storereview')->__('Status'),
            'index' => 'status',
            'type' => 'text',
        ));


        $this->addColumn('created_at', array(
            'header' => Mage::helper('inchoo_storereview')->__('Created At'),
            'index' => 'created_at',
            'type' => 'text',
            'sortable' => true
        ));


        $this->addColumn('updated_at', array(
            'header' => Mage::helper('inchoo_storereview')->__('Updated At'),
            'index' => 'updated_at',
            'type' => 'text',
            'sortable' => true
        ));


        $this->addColumn('action', array(
            'header' => Mage::helper('inchoo_storereview')->__('Action'),
            'type' => 'action',
            'getter' => 'getReviewId',
            'actions' => array(
                array(
                    'caption' => Mage::helper('inchoo_storereview')->__('Edit'),
                    'url' => array('base' => '*/*/edit'),
                    'field' => 'review_id'
                )
            )
        ));


        return parent::_prepareColumns();
    }

    /**
     * Return a URL to be used for each row
     *
     * If you don't wish rows to return a URL, simply omit this method
     *
     * @param Varien_Object $row The row for which to supply a URL
     *
     * @return string The row URL
     */
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('review_id' => $row->getId()));
    }


    /**
     * Prepare grid mass actions
     *
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('review_id');
        $this->getMassactionBlock()->setFormFieldName('review');
        $this->getMassactionBlock()->addItem(
            'delete',
            array(
                'label' => Mage::helper('inchoo_storereview')->__('Delete'),
                'url' => $this->getUrl('*/*/massDelete'),
                'confirm' => Mage::helper('inchoo_storereview')->__('Are you sure?')
            )
        );
        return $this;
    }
}