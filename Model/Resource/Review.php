<?php

/**
 * Class Inchoo_StoreReview_Model_Resource_Review
 */
class Inchoo_StoreReview_Model_Resource_Review extends Mage_Core_Model_Resource_Db_Abstract
{
    //THIS HANDLES CHECK WHEN INSERT/UPDATE AND SYSTEM DOES NOT KNOW SO I HAD TO UNSET ID FROM ARRAY WHEN CREATING NEW ONE
    protected $_isPkAutoIncrement = false;

    protected function _construct()
    {
        $this->_init('inchoo_storereview/review', 'review_id');
    }

}