<?php

/**
 * Class Inchoo_StoreReview_Model_Review
 */
class Inchoo_StoreReview_Model_Review extends Mage_Core_Model_Abstract
{

    /**
     * Approved review status code
     */
    const STATUS_APPROVED = 'Approved';

    /**
     * Pending review status code
     */
    const STATUS_PENDING = 'Pending';

    /**
     * Not Approved review status code
     */
    const STATUS_NOT_APPROVED = 'Not approved';

    /**
     * @var string
     */
    protected $_eventPrefix = 'inchoo_storereview/review';

    /**
     * @var string
     */
    protected $_eventObject = 'review';


    protected function _construct()
    {
        $this->_init('inchoo_storereview/review');
    }


    /**
     * Get review statuses
     *
     * @return array
     */
    public function getReviewStatuses()
    {
        return [
            self::STATUS_APPROVED => __('Approved'),
            self::STATUS_PENDING => __('Pending'),
            self::STATUS_NOT_APPROVED => __('Not Approved')
        ];
    }

    /**
     * Get review statuses as option array
     *
     * @return array
     */
    public function getReviewStatusesOptionArray()
    {
        $result = [];
        foreach ($this->getReviewStatuses() as $value => $label) {
            $result[] = ['value' => $value, 'label' => $label];
        }

        return $result;
    }


    /**
     * Get review ratings
     *
     * @return array
     */
    public static function getReviewRatings()
    {
        return [
            20 => __('1 star'),
            40 => __('2 stars'),
            60 => __('3 stars'),
            80 => __('4 stars'),
            100 => __('5 stars')
        ];
    }

    /**
     * Get review ratings as option array
     *
     * @return array
     */
    public function getReviewRatingsOptionArray()
    {
        $result = [];
        foreach (self::getReviewRatings() as $value => $label) {
            $result[] = ['value' => $value, 'label' => $label];
        }


        return $result;
    }

    /**
     * @param array $data
     * @throws Zend_Validate_Exception
     */
    public function validate(array $data)
    {

        if (!Zend_Validate::is($data['title'], 'NotEmpty')) {
            throw new \Zend_Validate_Exception(__('Title is missing'));
        }

        if (!Zend_Validate::is($data['description'], 'NotEmpty')) {
            throw new \Zend_Validate_Exception(__('Description is missing'));
        }

        if (!Zend_Validate::is($data['rating'], 'NotEmpty')) {
            throw new \Zend_Validate_Exception(__('Rating is missing'));
        }

    }

    /**
     * @return mixed
     */
    public function getAllCustomers()
    {

        return Mage::getModel('customer/customer')->getCollection()
            ->addAttributeToSelect('firstname')
            ->addAttributeToSelect('lastname')
            ->getItems();
    }


    /**
     * @return array
     */
    public function getCustomersToOptionsArray()
    {
        $result = [];
        foreach ($this->getAllCustomers() as $customer) {
            $result[] = ['value' => $customer->getId(), 'label' => $customer->getName()];
        }


        return $result;
    }

}