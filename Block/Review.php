<?php

use Inchoo_StoreReview_Model_Review as ReviewModel;

/**
 * Class Inchoo_StoreReview_Block_Review
 */
class Inchoo_StoreReview_Block_Review extends Mage_Core_Block_Template
{

    /**
     * @var array
     */
    protected $data = array();


    /**
     * Gets review object from registry
     * @return mixed
     */
    protected function getReview()
    {
        return Mage::registry('store_review');
    }


    /**
     * Decides which urls to return based on given value
     * @param Inchoo_StoreReview_Model_Review|null $review
     * @return array
     */
    public function getUrlStrategy(ReviewModel $review = null)
    {
        if (!$review->getId()) {

            $this->data[$this->getCreateUrl()] = __('Create Review');

        } else {

            $this->data[$this->getEditUrl()] = __('Edit Review');
            $this->data[$this->getDeleteUrl()] = __('Delete Review');
        }

        $this->data[$this->getBackUrl()] = __('Back');

        return $this->data;

    }


    /**
     * @return string
     */
    public function getCreateUrl()
    {
        return $this->getUrl('review/review/create/');
    }


    /**
     * @return string
     */
    public function getDeleteUrl()
    {
        return $this->getUrl('review/review/delete/');;
    }


    /**
     * @return string
     */
    public function getEditUrl()
    {
        return $this->getUrl('review/review/edit/');
    }


    /**
     * Return review customer url
     *
     * @return string
     */
    public function getBackUrl()
    {
        return $this->getUrl('review/review/show/');
    }


    /**
     * @return Mage_Core_Controller_Varien_Action|string
     * @throws Exception
     */
    public function getAction()
    {
        return $this->getUrl(
            'review/review/save',
            [
                '_secure' => $this->getRequest()->isSecure()
            ]
        );
    }

    /**
     * Format date in short format
     *
     * @param string $date
     * @return string
     */
    public function dateFormat($date)
    {
        return $this->formatDate($date, \IntlDateFormatter::SHORT, true);
    }

}