<?php

/**
 * Class Inchoo_StoreReview_Model_Observer
 */
class Inchoo_StoreReview_Model_Observer
{
    /**
     * Test observer class - used for sending emails - right now some things are hardcoded so it is not reliable - just for testing purpose
     * @param null $observer
     * @throws Mage_Core_Model_Store_Exception
     */
    public function intercept($observer = null)
    {

        $customer = Mage::getSingleton('customer/session')->getCustomer();

        $templateId = 1; // Enter you new template ID
        $senderName = Mage::getStoreConfig('trans_email/ident_support/name');  //Get Sender Name from Store Email Addresses
        $senderEmail = Mage::getStoreConfig('trans_email/ident_support/email');  //Get Sender Email Id from Store Email Addresses
        $sender = array('name' => $senderName,
            'email' => $senderEmail);

        $recepientEmail = $customer->getEmail();
        $recepientName = $customer->getName();

        $storeId = Mage::app()->getStore()->getId();

        $vars = array('customerName' => $customer->getName());

        Mage::getModel('core/email_template')
            ->sendTransactional($templateId, $sender, $recepientEmail, $recepientName, $vars, $storeId);


    }
}